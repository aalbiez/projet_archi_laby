KO=artefacts/spear.wav
OK=artefacts/heal.wav

run:
	@java -jar artefacts/Mars4_4.jar ae1 se1 nc source.s alea.s pa 5 && aplay -q $(OK) || aplay -q $(KO)
	@echo "__________________________________________________"

distrib:
	@zip -r labyrinthe.zip artefacts/* .editorconfig *.s Makefile print_maze.sh README.md watch generate rapport.*

clean:
	@rm .maze.txt labyrinthe.zip
