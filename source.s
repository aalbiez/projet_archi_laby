
# ----------------------------------------------------------------------------
# Macro d'affichage d'une chaine constante
.macro print_str (%str)
.data
  myLabel: .asciiz %str
.text
	li $v0, 4
	la $a0, myLabel
	syscall
.end_macro


# ----------------------------------------------------------------------------
# Macro d'affichage d'un entier
.macro print_int
  li $v0, 1
  syscall
.end_macro


# ----------------------------------------------------------------------------
# Macro d'affichage d'un entier padé avec un 0 si nécéssaire
.macro print_int_dd
  bge $a0, 10, print
  move $a1, $a0
  print_str("0")
  move $a0, $a1
print:
  li $v0, 1
  syscall
.end_macro


# ----------------------------------------------------------------------------
# Macro d'empilement de $ra
.macro push_ra
  subu $sp, $sp, 4
  sw $ra, ($sp)
.end_macro

# ----------------------------------------------------------------------------
# Macro de démpilement de $ra et return
.macro pop_ra_and_return
  lw $ra, ($sp)
  addu $sp, $sp, 4
  jr $ra
.end_macro


# ----------------------------------------------------------------------------
# Macro d'empilement de $ra
.macro push_saved(%v)
  subu $sp, $sp, 4
  sw %v, ($sp)
.end_macro

# ----------------------------------------------------------------------------
# Macro de démpilement de $ra et return
.macro pop_saved(%v)
  lw %v, ($sp)
  addu $sp, $sp, 4
.end_macro


# ----------------------------------------------------------------------------
# Macro d'affichage d'une chaine constante
.macro check_eq (%value, %expected, %message)
  add $t0, $zero, %expected       # Initialiser dans tO la valeur attendue
  beq %value, $t0, ok             # Si valeur == attendu, Ok
  print_str("FAIL: ")             # Sinon, afficher FAIL:
  print_str(%message)             # Puis message
  li $a0, 1                       # Préparer la code de sortie à 1
  li $v0, 17                      # Selectioner la fonction exit
  syscall                         # Quitter le programme
ok:
.end_macro


.text

__start:
  move $s0 $a0                    # Mettre dans $s0 argc
  move $s1 $a1                    # Mettre dans $s1 l'adresse de argv

  # Lance les fonctions d'autotest
  jal suite_utilitaires
  jal suite_cells
  jal suite_st
  jal suite_labyrinthe
  jal suite_generation_labyrinthe

  bne $s0 1 __print_usage         # Si argc != 1, afficher l'usage

  lw $a0 ($s1)                    # Récupérer argv[0]
  jal str_to_int                  # Convertir en entier
  move $a0, $v0                   # Préparer la génération du labyrinthe
  jal generer_labyrinthe          # Générer le labyrinthe
  move $a0, $v0                   # Préparer l'affichage du labyrinthe
  jal labyrinthe_afficher         # Afficher le labyrinthe
  j __end                         # Terminer

__print_usage:
  print_str("Usage: labyrinthe <N>")

__end:
  li $v0, 10                      # Selectioner la fonction exit
  syscall                         # Quitter le programme


# ============================================================================
# Utilitaires
# ============================================================================

# ----------------------------------------------------------------------------
# Conversion chaine vers entier
# /!\ pas de verification d'erreur
# $a0: l'adresse de la chaine terminée par 0
# $v0: l'entier correspondant
str_to_int:
  li $v0, 0                       # Initialiser le résultat à 0
  li $t1, 10                      # Charger 10 dans t0, pour la base décimale
sti_loop:
  lb $t0, ($a0)                   # Lire le caractère à l'adresse $a0
  beq $t0, 0, sti_end             # Si nul, finir
  subi $t0, $t0, 48               # Soustraire 48, car 0 est codé 48 an ascii
  mult $v0, $t1                   # Multiplier le resultat par 10
  mflo $v0                        # Récupérer dans $v0
  add $v0, $v0, $t0               # Ajouter le chiffre courant à $v0
  addi $a0, $a0, 1                # Passer au caractère suivant
  j sti_loop                      # Reboucler
sti_end:
  jr $ra                          # Retourner


# ============================================================================
# Test les fonction utilitaires

# ----------------------------------------------------------------------------
test_str_to_int:
.data
A: .asciiz ""
B: .asciiz "5"
C: .asciiz "42"
.text
  push_ra

  la $a0, A
  jal str_to_int
  check_eq ($v0, 0, "str_to_int('') aurait du être 0")

  la $a0, B
  jal str_to_int
  check_eq ($v0, 5, "str_to_int('5') aurait du être 5")

  la $a0, C
  jal str_to_int
  move $a0, $v0
  check_eq ($v0, 42, "str_to_int('42') aurait du être 42")

  pop_ra_and_return

# ----------------------------------------------------------------------------
suite_utilitaires:
  push_ra
  jal test_str_to_int
  pop_ra_and_return


# ============================================================================
# Gestion de la cellule
# ============================================================================

# Structure d'une cellule
#---------------------------------------------------------------------------
#   B7    |   B6  |   B5  |   B4    |   B3    |   B2   |   B1    |   B0    |
#---------------------------------------------------------------------------
# B0: mur en haut
# B1: mur droite
# B2: mur bas
# B3: mur gauche
# B4: départ
# B5: fin
# B6: visité

# ----------------------------------------------------------------------------
# Renvoie la valeur du <i> eme bit de <n>
# $a0 : entier <n>
# $a1 : index <i> du bit
# $v0 : la valeur du bit <i> de <n>
cell_lecture_bit:
                                  # Construire le masque binaire
  li $t0, 1                       #   Préparer le masque avec le bit 0 à 1
  sllv $t0, $t0, $a1              #   Décaler le masque de <i> vers la gauche,
                                  #   pour n'avoir que des sauf au bit <i>
                                  # Calculer le resultat
  and $t0, $t0, $a0               #   Mettre dans $tO l'état du <i> ème bit de <n>
  srlv $v0, $t0, $a1              #   Décaler vers la droite de <i> pour n'avoir 0 ou 1 comme resultat
  jr $ra                          # Retourner


# ----------------------------------------------------------------------------
# Mets à 1 le bit <i> de lentier <n>.
# $a0 : entier <n>
# $a1 : index <i> du bit
# $v0 : <n> avec le bit <i> mit à 1
cell_mettre_bit_a1:
                                  # Construire le masque binaire
  li $t0, 1                       #   Préparer le masque avec le bit 0 à 1
  sllv $t0, $t0, $a1              #   Décaler le masque de <i> vers la gauche,
                                  #   pour n'avoir que des 0 sauf au bit <i>
                                  # Calculer le resultat
  or $v0, $t0, $a0                #   Mettre à 1 le <i> ème bit
  jr $ra                          # Retourner


# ----------------------------------------------------------------------------
# Mets à 0 le bit <i> de lentier <n>.
# $a0 : entier <n>
# $a1 : index <i> du bit
# $v0 : <n> avec le bit <i> mit à 0
cell_mettre_bit_a0:
                                  # Construire le masque binaire
  li $t0, 1                       #   Préparer le masque avec le bit 0 à 1
  sllv $t0, $t0, $a1              #   Décaler le masque de <i> vers la gauche
  xori $t0, $t0, 0xFF             #   Inverser tous les bits du masque,
                                  #   tous les bits sont donc à 1 sauf le bit <i>
                                  # Calculer le resultat
  and $v0,$t0, $a0                #   Annuler le bit <i> de <n>
  jr $ra                          # Retourner


# ============================================================================
# Test les fonction de gestion de la cellule

# ----------------------------------------------------------------------------
test_cell_lecture_bit:
  push_ra

  li $a0, 29
  li $a1, 0
  jal cell_lecture_bit
  check_eq ($v0, 1, "le bit 0 de 29 aurait du être 1")

  li $a0, 29
  li $a1, 1
  jal cell_lecture_bit
  check_eq ($v0, 0, "le bit 1 de 29 aurait du être 0")

  pop_ra_and_return

# ----------------------------------------------------------------------------
test_cell_mettre_bit_a1:
  push_ra

  li $a0, 0x00
  li $a1, 1
  jal cell_mettre_bit_a1
  check_eq ($v0, 0x02, "0x00 avec le bit 1 mit à 1 aurait du donner 0x02")

  li $a0, 0x02
  li $a1, 1
  jal cell_mettre_bit_a1
  check_eq ($v0, 0x02, "0x02 avec le bit 1 mit à 1 aurait du donner 0x02")

  pop_ra_and_return

# ----------------------------------------------------------------------------
test_cell_mettre_bit_a0:
  push_ra

  li $a0, 0x02
  li $a1, 1
  jal cell_mettre_bit_a0
  check_eq ($v0, 0x00, "0x02 avec le bit 1 mit à 0 aurait du donner 0x00")

  li $a0, 0x00
  li $a1, 1
  jal cell_mettre_bit_a0
  check_eq ($v0, 0x00, "0x00 avec le bit 1 mit à 0 aurait du donner 0x00")

  pop_ra_and_return

# ----------------------------------------------------------------------------
suite_cells:
  push_ra
  jal test_cell_lecture_bit
  jal test_cell_mettre_bit_a1
  jal test_cell_mettre_bit_a0
  pop_ra_and_return


# ============================================================================
# Gestion de la pile
# ============================================================================


# Structure mémoire de la pile
#
# ----------------...
# | S | C | e1 | e2 | ...
# ----------------...
#   ^   ^   ^ Elements dans la pile 4*S octets
#   |   |- Nombre d'élément actuellement dans la pile sur 4 octets
#   |- Taille totale de la pile sur 4 octets
#
# Taille totale : 4 + 4 + S*4 octets


# ----------------------------------------------------------------------------
# Cree une pile de la taille donné
# $a0 : taille de la pile
# $v0 : l'adresse de la pile allouée
st_creer:
  move $t0, $a0                   # Copier dans t0 la taille de la pile
                                  # Calculer la taille de la pile en octets
  add $a0, $t0, 2                 #   Ajouter deux pour stoker S et C
  sll $a0, $a0, 2                 #   Multiplier par 4 avec un décalage à gauche de 2,
                                  #   la taille d'un entier est de 4 octets,
  li $v0, 9                       # Préparer l'appel system brsk
  syscall                         # Allouer la memoire
  sw $t0, ($v0)                   # Initialiser la taille totale de la pile
  li $t0, 0                       # Préparer le nombre actuel d'élément dans la pile
  sw $t0, 4($v0)                  # Initialiser à 0 le nombre d'éléments actuels dans la pile
  jr $ra                          # Retourner


# ----------------------------------------------------------------------------
# Test si la pile est vide
# $a0 : Adresse de la pile
# $v0 : 1 si la pile est effectivement vide et 0 si elle contient au moins un élément
st_est_vide:
  lw $t0, 4($a0)                  # Recupérer le nombre d'éléments actuellement dans la pile
  bne $t0, 0, sev_faux            # Si différent de zero, sauter à la pile n'est pas vide

sev_vrai:                         # La pile est vide
  li $v0, 1                       #   Le résultat est 1
  jr $ra                          #   Retourner

sev_faux:                         # La pile n'est pas vide
  li $v0, 0                       #   Le résultat est 0
  jr $ra                          #   Retourner


# ----------------------------------------------------------------------------
# Test si la pile est pleine
# $a0 : Adresse de la pile
# $v0 : 1 si la pile est pleine et 0 si elle peut encore empiler des elements
st_est_pleine:
  lw $t0, ($a0)                   # Recupérer la taille totale de la pile
  lw $t1, 4($a0)                  # Recupérer le nombre d'éléments actuellement dans la pile
  bne $t0, $t1, sep_faux          # Si différent alors la pile n'est pas pleine

sep_vrai:                         # La pile est pleine
  li $v0, 1                       #   Le résultat est 1
  jr $ra                          #   Retourner

sep_faux:                         # La pile n'est pas pleine.
  li $v0, 0                       #   Le résultat est 0
  jr $ra                          #   Retourner


# ----------------------------------------------------------------------------
# Macro calcul dans %addr l'adresse du %i-ème element de %stack
.macro st_addr_element (%stack, %i, %addr)
  sll %addr, %i, 2                # Multiplie par 4 l'index de l'élément à chercher
                                  # pour avoir un offset en octets.
  add %addr, %addr, %stack        # Ajoute l'adresse de la pile
  addu %addr, %addr, 8            # Décale de 8 octets pour passer les deux premiers
                                  # champs de la structure
.end_macro


# ----------------------------------------------------------------------------
# Renvoie le sommet de la pile
# $a0 : Adresse de la pile
# $v0 : valeur du sommet de la pile
st_sommet:
  lw $t0, 4($a0)                  # Recupère le nombre d'element actuel de la pile.
  subi $t0, $t0, 1                # Soustraire 1 pour avoir un offset qui commence à 0.
  st_addr_element($a0, $t0, $t1)  # Calcule l'adresse du dernier élément de la pile
  lw $v0, ($t1)                   # Que l'on charge
  jr $ra                          # Retourner


# ----------------------------------------------------------------------------
# Ajoute dans la pile un nouvel element
# $a0: Adresse de la pile
# $a1: E, l'élement a rajouter dans la pile
st_empiler:
  lw $t0, 4($a0)                  # Recupère le nombre d'element actuel de la pile.
  st_addr_element($a0, $t0, $t1)  # Calcule l'adresse du dernier élément de la pile
  sw $a1, ($t1)                   # Stock l'élément à ajouter dans la pile.
  addi $t0, $t0, 1                # Incrémente le nombre d'éléments dans la pile.
  sw $t0, 4($a0)                  # Mettre à jour le nombre d'éléments dans la pile.
  jr $ra                          # Retourner


# ----------------------------------------------------------------------------
# Enleve le dernier élément de la pile
# $a0: Adresse de la pile
st_depiler:
  lw $t1, 4($a0)                  # Recupère le nombre d'element actuel de la pile.
  subi $t1, $t1, 1                # Décrémente de 1
  sw $t1, 4($a0)                  # Enregistre le nouveau nombre d'element dans la pile
  jr $ra			                    # Retourner


# ----------------------------------------------------------------------------
# Affiche la pile
# $a0: Adresse de la pile
st_afficher:
  move $t0, $a0                   # Sauvegarder l'adresse de la pile dans t0
  print_str("[ ")                 # Afficher le '[ ' initial
  lw $a0, ($t0)                   # Récupérer la taille totale de la pile.
  print_int                       # L'afficher
  print_str(" : ")                # Afficher ls séparateur ' : '
  addu $t2, $t0, 8                # Décaler l'adresse du premier élément
  lw $t1, 4($t0)                  # Récupérer le nombre d'éléments actuellement dans la pile
sa_loop:
  ble $t1, 0, sa_end              # Si le nombre d'élément actuellement dans la pile est nul, finir
  lw $a0, ($t2)                   # Charger l'élément courant
  print_int                       # Afficher
  print_str(" ")                  # Afficher un espace
  subu $t1, $t1, 1                # Décrémenter le nombre d'élément dans la pile
  addu $t2, $t2, 4                # Passer à l'adresse de l'élément suivant
  j sa_loop                       # Boucler
sa_end:
  print_str("]")                  # Afficger ']'
  jr $ra                          # Retourner


# ============================================================================
# Test les fonction de gestion de la pile

# ----------------------------------------------------------------------------
test_st_est_vide:
  push_ra
  push_saved($s0)

  li $a0, 2
  jal st_creer
  move $s0, $v0

  move $a0, $s0
  jal st_est_vide
  check_eq ($v0, 1, "la pile aurait du être vide")

  li $a1, 42
  jal st_empiler

  jal st_est_vide
  check_eq ($v0, 0, "la pile n'aurait pas du être vide")
  jal st_depiler

  jal st_est_vide
  check_eq ($v0, 1, "la pile aurait du être vide")

  pop_saved($s0)
  pop_ra_and_return

# ----------------------------------------------------------------------------
test_st_est_pleine:
  push_ra
  push_saved($s0)

  li $a0, 2
  jal st_creer
  move $s0, $v0

  move $a0, $s0
  jal st_est_pleine
  check_eq ($v0, 0, "la pile n'aurait pas du être pleine")

  move $a0, $s0
  li $a1, 42
  jal st_empiler

  move $a0, $s0
  li $a1, 43
  jal st_empiler

  move $a0, $s0
  jal st_est_pleine
  check_eq ($v0, 1, "la pile aurait du être pleine")

  move $a0, $s0
  jal st_depiler

  move $a0, $s0
  jal st_est_pleine
  check_eq ($v0, 0, "la pile n'aurait pas du être pleine")

  pop_saved($s0)
  pop_ra_and_return


# ----------------------------------------------------------------------------
test_st_sommet:
  push_ra
  push_saved($s0)

  li $a0, 2
  jal st_creer
  move $s0, $v0

  move $a0, $s0
  li $a1, 42
  jal st_empiler

  move $a0, $s0
  jal st_sommet
  check_eq ($v0, 42, "la sommet de pile aurait du être 42")

  move $a0, $s0
  li $a1, 43
  jal st_empiler

  move $a0, $s0
  jal st_sommet
  check_eq ($v0, 43, "la sommet de pile aurait du être 43")

  move $a0, $s0
  jal st_depiler
  move $a0, $s0
  jal st_sommet
  check_eq ($v0, 42, "la sommet de pile aurait du être 42")

  pop_saved($s0)
  pop_ra_and_return


# ----------------------------------------------------------------------------
# Test les fonction de gestion de la pile
suite_st:
  push_ra
  jal test_st_est_vide
  jal test_st_est_pleine
  jal test_st_sommet
  pop_ra_and_return


# ============================================================================
# Gestion du labyrinthe
# ============================================================================


# Structure mémoire du labyrinthe
#
# ------------...
# | N | e1 | e2 | ...
# ------------...
#   ^   ^ Cases du labyrinthe N*N octets
#   |- Taille du labyrinthe sur 4 octets
#
# Taille totale : 4 + N*N octets

.eqv Haut     1
.eqv Droite   2
.eqv Bas      3
.eqv Gauche   4

.eqv BitMurHaut     0
.eqv BitMurDroite   1
.eqv BitMurBas      2
.eqv BitMurGauche   3
.eqv BitDepart      4
.eqv BitFin         5
.eqv BitVisite      6


# ----------------------------------------------------------------------------
# Macro de calcul (ligne, colonne) -> indice
.macro vers_indice (%ligne, %colonne, %N, %indice)
  multu %ligne, %N
  mflo %indice
  addu %indice, %indice, %colonne # indice = ligne*N + colonne
.end_macro


# ----------------------------------------------------------------------------
# Macro de calcul indice -> (ligne, colonne)
.macro depuis_indice (%indice, %N, %ligne, %colonne)
  divu %indice, %N
  mflo %ligne                     # ligne = indice / N
  mfhi %colonne                   # colonne = indice % N
.end_macro


# ----------------------------------------------------------------------------
# Creer un nouveau labyrinthe vide
# $a0 : taille N du labyrinthe
labyrinthe_creer:
  move $t0, $a0                   # Sauvegarder N dans $t0
                                  # Calculer la taille de la structure du labyrinthe
  mult $a0, $a0                   #   Calculer le nombre de cellules : N*N
  mflo $a0
  move $t1, $a0                   #   Sauvegarder le nombre total de cellules dans $t1
  sll $a0, $a0, 2                 #   Multiplier par 4 pour avoir la taille en octets
  add $a0, $a0, 4                 #   Rajouter 4 pour le N dans notre structure
  li $v0, 9                       # Préparer l'appel sbrk
  syscall                         # Allouer la mémoire
  sw $t0, ($v0)                   # Initialiser la valeur de N dans la structure
                                  # Préparer la boucle
  # $t1                           #   Nombre total de cellules
  addi $t2, $v0, 4                #   Initialiser l'adresse de la cellule courante
                                  #   sur la première cellule
  li $t3, 15                      #   Préparaer la valeur par défaut d'une cellule à 4 murs
lc_loop:                          # Boucler sur toutes les cellules
  beq $t1, 0, lc_end              # Si il n'y as plus de cellule, finir
  sb $t3, ($t2)                   #   Initialiser la cellule courante
  addi $t2, $t2, 1                #   Passer à l'adresse de la cellule suivante
  subi $t1, $t1, 1                #   Décrémenter le nombre de cellules
  j lc_loop                       #   Reboucler
lc_end:
  jr $ra                          # Retourner


# ----------------------------------------------------------------------------
# Supprime le bit de visite du labyrinthe
# $a0 : adresse du labyrinthe
labyrinthe_nettoyer:
  move $t0, $a0                   # Sauvegarder l'adresse du labyrinthe dans $t0
  lw $t1, ($t0)                   # Récupérer la taille N du labyrinthe
  mult $t1, $t1                   # Calculer du nombre de cellules : N*N
  mflo $t2                        # Mettre le nombre totale de cellules dans $t2
  addu $t3, $t0, 4                # Décaler sur l'adresse de la première cellule
ln_loop:
  lb $t4, ($t3)                   # Charger la cellule courante
  andi $t4, $t4, 63				        # Effacer les bits 6 et 7
  sb $t4, ($t3)                   # Sauver la cellule courante
  addu $t3, $t3, 1                # Passer à l'adresse de la cellule suivante
  subi $t2, $t2, 1                # Décrémenter le nombre de cellules
  beq $t2, 0, ln_fin              # Si plus de cellules, finir
  j ln_loop                       # Reboucler
ln_fin:
  jr $ra			                    # Retourner


# ----------------------------------------------------------------------------
# Affiche du labyrinthe
# $a0 : adresse du labyrinthe
labyrinthe_afficher:
  # t0: adresse du labyrinthe
  # t1: N
  # t2: adresse de la cellule courante
  # t3: colonne de la cellule courante
  # t4: ligne de la cellule courante
  move $t0, $a0                   # Sauvegarder l'adresse du labyrinthe dans $t0
  lw $t1, ($t0)                   # Récupèrer la taille N du labyrinthe
  move $a0, $t1                   # Préparer l'affichage de N
  print_int_dd                    # Afficher N
  print_str("\n")                 # Passer à la ligne
  addu $t2, $t0, 4                # Décaler sur l'adresse de la première cellule
  li $t3, 0                       # Démarrer en colonne 0
  li $t4, 0                       # Et ligne 0
la_loop:
  lb $a0, ($t2)                   # Charger la cellule courante
  print_int_dd                    # L'afficher
  print_str(" ")                  # Afficher un espace
  addu $t2, $t2, 1                # Passer à l'adresse de la cellule suivante
  addu $t3, $t3, 1                # Incrémenter la colonne
  beq $t3, $t1, la_next_line      # Si la colonne est égal à N, passer à la ligne suivante
  j la_loop                       # Reboucler
la_next_line:
  print_str("\n")                 # Afficher un retour à la ligne
  addu $t4, $t4, 1                # Incrémenter la ligne
  li $t3, 0                       # Redémarrer en colonne 0
  beq $t4, $t1, la_fin            # Si la ligne est égal à N, finir
  j la_loop                       # Reboucler
la_fin:
  jr $ra			                    # Retourner


# ----------------------------------------------------------------------------
# Lire la cellule demander
# $a0 : Adresse du labyrinthe
# $a1 : Indice de la cellule demandé
# $v0 : La valeur de la cellule
labyrinthe_lire_cellule:
  addu $a0, $a0, 4                # Décaler sur l'adresse de la première cellule
  addu $a0, $a0, $a1              # Décaler sur l'adresse de la cellule demandé
  lb $v0, ($a0)                   # La lire
  jr $ra                          # Retourner


# ----------------------------------------------------------------------------
# Modifie la cellule demander
# $a0 : Adresse du labyrinthe
# $a1 : Indice de la cellule demandé
# $a2 : La nouvelle valeur de la cellule
labyrinthe_ecrire_cellule:
  addu $a0, $a0, 4                # Décaler sur l'adresse de la première cellule
  addu $a0, $a0, $a1              # Décaler sur l'adresse de la cellule demandé
  sb $a2, ($a0)                   # L'écrire
  jr $ra                          # Retourner


# ----------------------------------------------------------------------------
# Donne l'indice de la cellule voisine pour la direction spécifiée
# $a0 : Adresse du labyrinthe
# $a1 : Indice de la cellule
# $a2 : Direction haut, droite, bas, gauche
# $v0 : Indice de la cellulle voisine , -1 si non valide
labyrinthe_voisin_vers:
  # t0 = Taille N du labyrinthe
  # t1 = ligne
  # t2 = colonne
  # t3 = N-1
  lw $t0, ($a0)                   # Récupérer la taille N du labyrinthe
  subi $t3, $t0, 1                # Décrémenter N, car les valeures sont dans [0, N-1]
  depuis_indice($a1, $t0, $t1, $t2) # Calculer ligne,colonne depuis l'indice
                                  # Séléctionner en fonction de la direction
  beq $a2, Haut, lvv_haut         #   Haut
  beq $a2, Droite, lvv_droite     #   Droite
  beq $a2, Bas, lvv_bas           #   Bas
  beq $a2, Gauche, lvv_gauche     #   Gauche
  j lvv_paf                       # Si direction est invalide, paf
lvv_haut:
  beq $t1, 0, lvv_paf             # Si on est déjà en haut, paf
  subi $t1, $t1, 1                # Décrémenter ligne
  j lvv_fin                       # Finir
lvv_droite:
  beq $t2, $t3, lvv_paf           # Si on est déjà le plus à droite, paf
  addi $t2, $t2, 1                # Incrémenter colonne
  j lvv_fin                       # Finir
lvv_bas:
  beq $t1, $t3, lvv_paf           # Si on est déjà en bas, paf
  addi $t1, $t1, 1                # Incrémenter ligne
  j lvv_fin                       # Finir
lvv_gauche:
  beq $t2, 0, lvv_paf             # Si on est déjà à gauche, paf
  subi $t2, $t2, 1                # Décrémenter colonne
  j lvv_fin                       # Finir
lvv_fin:
  vers_indice($t1, $t2, $t0, $v0) # Recalculer l'indice avec les nouvelles coordonnées
  jr $ra                          # Retourner
lvv_paf:
  li $v0, -1                      # Si direction est invalide, retourner -1
  jr $ra                          # Retourner


# ============================================================================
# Test les fonction de gestion du labyrinthe

# ----------------------------------------------------------------------------
test_labyrinthe_lire_cellule:
  push_ra
  push_saved($s0)

  li $a0, 2
  jal labyrinthe_creer
  move $s0, $v0

  move $a0, $s0
  li $a1, 2
  jal labyrinthe_lire_cellule
  check_eq ($v0, 15, "la cellule aurait du valoir 15")

  pop_saved($s0)
  pop_ra_and_return

# ----------------------------------------------------------------------------
test_labyrinthe_ecrire_cellule:
  push_ra
  push_saved($s0)

  li $a0, 2
  jal labyrinthe_creer
  move $s0, $v0

  move $a0, $s0
  li $a1, 2
  li $a2, 42
  jal labyrinthe_ecrire_cellule

  move $a0, $s0
  li $a1, 2
  jal labyrinthe_lire_cellule
  check_eq ($v0, 42, "la cellule aurait du valoir 42")

  pop_saved($s0)
  pop_ra_and_return

# ----------------------------------------------------------------------------
test_depuis_indice:
  push_ra

  li $t1, 4
  li $t2, 3
  depuis_indice($t1, $t2, $v0, $v1)
  check_eq ($v0, 1, "la ligne devrait valoir 1")
  check_eq ($v1, 1, "la colonne devrait valoir 1")

  pop_ra_and_return

# ----------------------------------------------------------------------------
test_vers_indice:
  push_ra

  li $t1, 1
  li $t2, 1
  li $t3, 3
  vers_indice($t1, $t2, $t3, $v0)
  check_eq ($v0, 4, "lindice devrait valoir 4")

  pop_ra_and_return

# ----------------------------------------------------------------------------
test_labyrinthe_voisin_vers:
  push_ra
  push_saved($s0)

  li $a0, 3
  jal labyrinthe_creer
  move $s0, $v0

  # Test voisin haut
  move $a0, $s0
  li $a1, 4
  li $a2, Haut
  jal labyrinthe_voisin_vers
  check_eq ($v0, 1, "l'indice aurait du valoir 1")

  # Test voisin droite
  move $a0, $s0
  li $a1, 4
  li $a2, Droite
  jal labyrinthe_voisin_vers
  check_eq ($v0, 5, "l'indice aurait du valoir 2")

  # Test voisin bas
  move $a0, $s0
  li $a1, 4
  li $a2, Bas
  jal labyrinthe_voisin_vers
  check_eq ($v0, 7, "l'indice aurait du valoir 7")

  # Test voisin gauche
  move $a0, $s0
  li $a1, 4
  li $a2, Gauche
  jal labyrinthe_voisin_vers
  check_eq ($v0, 3, "l'indice aurait du valoir 3")

  # Test voisin haut, paf
  move $a0, $s0
  li $a1, 0
  li $a2, Haut
  jal labyrinthe_voisin_vers
  check_eq ($v0, -1, "l'indice aurait du valoir -1")

  # Test voisin gauche, paf
  move $a0, $s0
  li $a1, 0
  li $a2, Gauche
  jal labyrinthe_voisin_vers
  check_eq ($v0, -1, "l'indice aurait du valoir -1")

  # Test voisin droite, paf
  move $a0, $s0
  li $a1, 8
  li $a2, Droite
  jal labyrinthe_voisin_vers
  check_eq ($v0, -1, "l'indice aurait du valoir -1")

  # Test voisin bas, paf
  move $a0, $s0
  li $a1, 8
  li $a2, Bas
  jal labyrinthe_voisin_vers
  check_eq ($v0, -1, "l'indice aurait du valoir -1")

  pop_saved($s0)
  pop_ra_and_return

# ----------------------------------------------------------------------------
suite_labyrinthe:
  push_ra
  jal test_labyrinthe_lire_cellule
  jal test_labyrinthe_ecrire_cellule
  jal test_depuis_indice
  jal test_vers_indice
  jal test_labyrinthe_voisin_vers
  pop_ra_and_return


# ============================================================================
# Génération du labyrinthe
# ============================================================================

# ----------------------------------------------------------------------------
# Modifie la cellule indiqué pour être une cellule de départ
# $a0 : Adresse du labyrinthe
# $a1 : Indice de la cellule à changer
definir_cellule_depart:
  push_ra                         # Sauvegarder $ra
  push_saved($s0)                 # Sauvegarder $s0
  push_saved($s1)                 # Sauvegarder $s1
  move $s0, $a0                   # Mettre dans $s0 l'adresse du labyrinthe
  move $s1, $a1                   # Mettre dans $s1 l'indice de la cellule à changer
  jal labyrinthe_lire_cellule     # Lire la valeur de la cellule
  move $a0, $v0                   # Préparer dans $a0 le contenu de la cellule
  li $a1, BitDepart               # Selectionner bit de départ
  jal cell_mettre_bit_a1          # Le mettre à 1
  move $a0, $s0                   # Préparer dans $a0 l'adresse du labyrinthe
  move $a1, $s1                   # Préparer dans $a1 l'indice de la cellule à changer
  move $a2, $v0                   # Préparer dans $a2 la nouvelle valeur de la cellule
  jal labyrinthe_ecrire_cellule   # Ecrire la cellule
  pop_saved($s1)                  # Restaurer $s1
  pop_saved($s0)                  # Restaurer $s0
  pop_ra_and_return               # Restaurer $ra et retourner


# ----------------------------------------------------------------------------
# Modifie la cellule indiqué pour être une cellule de fin
# $a0 : Adresse du labyrinthe
# $a1 : Indice de la cellule à changer
definir_cellule_fin:
  push_ra                         # Sauvegarder $ra
  push_saved($s0)                 # Sauvegarder $s0
  push_saved($s1)                 # Sauvegarder $s1
  move $s0, $a0                   # Mettre dans $s0 l'adresse du labyrinthe
  move $s1, $a1                   # Mettre dans $s1 l'indice de la cellule à changer
  jal labyrinthe_lire_cellule     # Lire la valeur de la cellule
  move $a0, $v0                   # Préparer dans $a0 le contenu de la cellule
  li $a1, BitFin                  # Selectionner le bit de fin
  jal cell_mettre_bit_a1          # Le mettre à 1
  move $a0, $s0                   # Préparer dans $a0 l'adresse du labyrinthe
  move $a1, $s1                   # Préparer dans $a1 l'indice de la cellule à changer
  move $a2, $v0                   # Préparer dans $a2 la nouvelle valeur de la cellule
  jal labyrinthe_ecrire_cellule   # Ecrire la cellule
  pop_saved($s1)                  # Restaurer $s1
  pop_saved($s0)                  # Restaurer $s0
  pop_ra_and_return               # Restaurer $ra et retourner


# ----------------------------------------------------------------------------
# Modifie la cellule indiqué pour signalée qu'elle a été visitée
# $a0 = Adresse du labyrinthe
# $a1 = Indice de l'element a changer
definir_cellule_visitee:
  push_ra                         # Sauvegarder $ra
  push_saved($s0)                 # Sauvegarder $s0
  push_saved($s1)                 # Sauvegarder $s1
  move $s0, $a0                   # Mettre dans $s0 l'adresse du labyrinthe
  move $s1, $a1                   # Mettre dans $s1 l'indice de la cellule à changer
  jal labyrinthe_lire_cellule     # Lire la valeur de la cellule
  move $a0, $v0                   # Préparer dans $a0 le contenu de la cellule
  li $a1, BitVisite               # Selectionner le bit visité
  jal cell_mettre_bit_a1          # Le mettre à 1
  move $a0, $s0                   # Préparer dans $a0 l'adresse du labyrinthe
  move $a1, $s1                   # Préparer dans $a1 l'indice de la cellule à changer
  move $a2, $v0                   # Préparer dans $a2 la nouvelle valeur de la cellule
  jal labyrinthe_ecrire_cellule   # Ecrire la cellule
  pop_saved($s1)                  # Restaurer $s1
  pop_saved($s0)                  # Restaurer $s0
  pop_ra_and_return               # Restaurer $ra et retourner


# ----------------------------------------------------------------------------
# Test si une celulle a déjà été visitée
# $a0 = Adresse du labyrinthe
# $a1 = Indice de la celulle à tester
celulle_est_deja_visite:
  push_ra                         # Sauvegarder $ra
  jal labyrinthe_lire_cellule     # Lire la valeur de la cellule
  move $a0, $v0                   # Met la valeur de la cellule dans $a0
  li $a1, BitVisite               # Mets dans a1 le bit de visite
  jal cell_lecture_bit            # Lit le bit
  pop_ra_and_return               # Restaurer $ra et retourner


# ----------------------------------------------------------------------------
# Liste les directions valides de l'indice donné
#   Une direction valide doit :
#     1. ne pas sortir du labyrinthe
#     2. ne pas avoir déjà été visité
# $a0 : Adresse du labyrinthe
# $a1 : Indice de la celulle courante
# $v0 : Champ de bits : B0: haut, B1: droite, B2: bas, B3 :gauche
lister_valides_directions:
  push_ra                         # Sauvegarder $ra
  push_saved($s0)                 # Sauvegarder $s0
  push_saved($s1)                 # Sauvegarder $s1
  push_saved($s2)                 # Sauvegarder $s2
  move $s0, $a0                   # $s0 = adresse du labyrinthe
  move $s1, $a1                   # $s1 = indice de la celulle courante
  li $s2, 0                       # $s2 = champs de bits, initialisé à 0

  move $a0, $s0                   # Préparer l'adresse du labyrinthe dans $a0
  move $a1, $s1                   # Préparer l'indice de la cellule courante dans $a1
  li $a2, 1                       # Verifier le voisin en haut
  jal labyrinthe_voisin_vers      # Calculer l'indice du voisin
  beq $v0, -1, lgdv_droite        # Si pas de voisin, passer à la direction suivante
  move $a0, $s0                   # Préparer l'adresse du labyrinthe dans $a0
  move $a1, $v0                   # Préparer l'indice de la cellule voisine dans $a1
  jal celulle_est_deja_visite     # Vérifier si le voisin a déjà été visité
  beq $v0, 1, lgdv_droite         # Si déjà visité, passer à la direction suivante
  ori $s2, $s2, 1			            # Activer le bit 0 du champ de bits
lgdv_droite:
  move $a0, $s0                   # Préparer l'adresse du labyrinthe dans $a0
  move $a1, $s1                   # Préparer l'indice de la cellule courante dans $a1
  li $a2, 2                       # Verifier le voisin à droite
  jal labyrinthe_voisin_vers      # Calculer l'indice du voisin
  beq $v0, -1, lgdv_bas           # Si pas de voisin, passer à la direction suivante
  move $a0, $s0                   # Préparer l'adresse du labyrinthe dans $a0
  move $a1, $v0                   # Préparer l'indice de la cellule voisine dans $a1
  jal celulle_est_deja_visite     # Vérifier si le voisin a déjà été visité
  beq $v0, 1, lgdv_bas            # Si déjà visité, passer à la direction suivante
  ori $s2, $s2, 2			            # Activer le bit 1 du champ de bits
lgdv_bas:
  move $a0, $s0                   # Préparer l'adresse du labyrinthe dans $a0
  move $a1, $s1                   # Préparer l'indice de la cellule courante dans $a1
  li $a2, 3                       # Verifier le voisin du bas
  jal labyrinthe_voisin_vers      # Calculer l'indice du voisin
  beq $v0, -1, lgdv_gauche        # Si pas de voisin, passer à la direction suivante
  move $a0, $s0                   # Préparer l'adresse du labyrinthe dans $a0
  move $a1, $v0                   # Préparer l'indice de la cellule voisine dans $a1
  jal celulle_est_deja_visite     # Vérifier si le voisin a déjà été visité
  beq $v0, 1, lgdv_gauche         # Si déjà visité, passer à la direction suivante
  ori $s2, $s2, 4			            # Activer le bit 2 du champ de bits
lgdv_gauche:
  move $a0, $s0                   # Préparer l'adresse du labyrinthe dans $a0
  move $a1, $s1                   # Préparer l'indice de la cellule courante dans $a1
  li $a2, 4                       # Verifier le voisin de gauche
  jal labyrinthe_voisin_vers      # Calculer l'indice du voisin
  beq $v0, -1, lgdv_end           # Si pas de voisin, finir
  move $a0, $s0                   # Préparer l'adresse du labyrinthe dans $a0
  move $a1, $v0                   # Préparer l'indice de la cellule voisine dans $a1
  jal celulle_est_deja_visite     # Vérifier si le voisin a déjà été visité
  beq $v0, 1, lgdv_end            # Si déjà visité, passer à la direction suivante
  ori $s2, $s2, 8			            # Activer le bit 3 du champ de bits
lgdv_end:
  move $v0, $s2                   # Retourner le champ de bits
  pop_saved($s2)                  # Restaurer $s2
  pop_saved($s1)                  # Restaurer $s1
  pop_saved($s0)                  # Restaurer $s0
  pop_ra_and_return               # Restaurer $ra et retourner


# ----------------------------------------------------------------------------
# Macro d'inversion de la direction
.macro inverser_direction (%direction)
  # 1 (Haut) -> 3 (Bas)
  # 2 (Droite) -> 4 (Gauche)
  # 3 (Bas) -> 1 (Haut)
  # 4 (Gauche) -> 2 (Droite)
  # ((d+1) % 4) + 1
  addi %direction, %direction, 2
  blt %direction, 5, end
  subi %direction, %direction, 4
end:
.end_macro


# ----------------------------------------------------------------------------
# Casse un murs entre deux cellules
# $a0 : Adresse du labyrinthe
# $a1 : Indice de la cellule
# $a2 : Direction 1: haut, 2: droite, 3: bas, 4: gauche
# $v0 : Indice de la cellule voisine
casser_mur:
  push_ra                         # Sauvegarder $ra
  push_saved($s0)                 # Sauvegarder $s0
  push_saved($s1)                 # Sauvegarder $s1
  push_saved($s2)                 # Sauvegarder $s2
  push_saved($s3)                 # Sauvegarder $s3
  move $s0, $a0                   # Sauver l'adresse du labyrinthe dans $s0
  move $s1, $a1                   # Sauver l'indice de la cellue dans $s1
  move $s2, $a2                   # Sauver la direction dans $s2
                                  # Casser le mur de la cellule d'origine
  jal labyrinthe_lire_cellule     #   Lire la valeur de la cellule
  move $a0, $v0                   #   Préparer la valeur de la cellule dans $a0
  subi $a1, $a2, 1                #   Transformer la direction en indice de bit
  jal cell_mettre_bit_a0          #   Mettre à 0 le bit du mur dans la direction donée
  move $a0, $s0                   #   Préparer l'adresse du labyrinthe dans $a0
  move $a1, $s1                   #   Préparer l'indice de la cellule dans $a1
  move $a2, $v0                   #   Préparer le nouveau contenu de la cellule dans $a2
  jal labyrinthe_ecrire_cellule   #   Ecrire la valeur de la cellule
                                  # Casser le mur de la cellule voisine
  move $a0, $s0                   #   Préparer l'adresse du labyrinthe dans $a0
  move $a1, $s1                   #   Préparer l'indice de la cellule dans $a1
  move $a2, $s2                   #   Préparer la direction dans $a2
  jal labyrinthe_voisin_vers      #   Calculer de l'indice du voisin
  move $s3, $v0                   #   Sauver l'indice du voisin dans $s3
  move $a0, $s0                   #   Préparer l'adresse du labyrinthe dans $a0
  move $a1, $s3                   #   Préparer l'indice de la cellule voisine dans $a1
  jal labyrinthe_lire_cellule     #   Lire la valeur de la cellule voisine
  move $a0, $v0                   #   Préparer la valeur de la cellule dans $a0
  move $a1, $s2                   #   Préparer la direction dans $a1
  inverser_direction($a1)         #   Inverser la direction
  subi $a1, $a1, 1                #   Transformer la direction en indice de bit
  jal cell_mettre_bit_a0          #   Mettre à 0 le bit du mur dans la direction donée
  move $a0, $s0                   #   Préparer l'adresse du labyrinthe dans $a0
  move $a1, $s3                   #   Préparer l'indice de la cellule voisine dans $a1
  move $a2, $v0                   #   Préparer le nouveau contenu de la cellule voisine dans $a2
  jal labyrinthe_ecrire_cellule   #   Ecrire la valeur de la cellule voisine
  move $v0, $s3                   # Renvoyer l'indice de la cellule voisine
  pop_saved($s3)                  # Restaurer $s3
  pop_saved($s2)                  # Restaurer $s2
  pop_saved($s1)                  # Restaurer $s1
  pop_saved($s0)                  # Restaurer $s0
  pop_ra_and_return               # Restaurer $ra et retourner


# ----------------------------------------------------------------------------
# Renvoie une direction aléatoire parmis les voisins valides
# $a0 : champ de bit pour liste des directions valide
# $v0 : Direction 1: haut, 2: droite, 3: bas, 4: gauche, 0 pas de voisin valide
voisin_aleatoire:
  push_ra                         # Sauvegarder $ra
  push_saved($s0)                 # Sauvegarder $s0
  push_saved($s1)                 # Sauvegarder $s1
  move $s0, $a0                   # Sauvegarder le champ de bit dans $s0
  li $v0, 0                       # Initialiser le resultat à 0 : pas de voisin valide
  beq $a0, 0, v_end               # Si il n'y a aucune direction valide, finir
v_loop:                           # Debut de la boucle
  li $a0, 0                       # Définir la borne min à 0
  li $a1, 4                       # Définir la borne max à 4
  jal nombre_alea_entre_deux_bornes# Tirer un nombre aleatoire dans [0; 4[
  move $s1, $v0                   # Sauvegarder le resultat dans $s1
  move $a0, $s0                   # Mettre le champ de bit dans $a0
  move $a1, $v0                   # Mettre le nombre aléatoire dans $a1
  jal cell_lecture_bit            # Lire le bit aléatoire dans la champ de bit
  move $t0, $v0                   # Déplacer la valeur du bit dans $t0
  addi $v0, $s1, 1                # Mettre dans $v0 la direction potentielle
  beq $t0, 1, v_end               # Si le bit lu est 1, une direction valide est trouvée, finir
  j v_loop                        # Sinon, recommencer
v_end:                            # Finir
  pop_saved($s1)                  # Restaurer $s1
  pop_saved($s0)                  # Restaurer $s0
  pop_ra_and_return               # Restaurer $ra et retourner


# ----------------------------------------------------------------------------
# Genere un nouveau labyrinthe aléatoire de taille N
# $a0 : Taille N du labyrinthe
generer_labyrinthe:
  push_ra                         # Sauvegarder $ra
  push_saved($s0)                 # Sauvegarder $s0
  push_saved($s1)                 # Sauvegarder $s1
  push_saved($s2)                 # Sauvegarder $s2
  push_saved($s3)                 # Sauvegarder $s3
  push_saved($s4)                 # Sauvegarder $s4

  # $s0 = taille N du labyrinthe
  # $s1 = N*N
  # $s2 = adresse du labyrinthe
  # $s3 = adresse de la pile
  # $s4 = indice de la celulle courante

  move $s0, $a0                   # Sauvegarder N dans $s0
  multu $s0, $s0                  # Calculer de N*N
  mflo $s1                        # Dans $s1

  # 1. Créer un labyrinthe avec des cellules qui ont initialement chacune 4 murs ;
  jal labyrinthe_creer            # Créer le labyrinthe vide
  move $s2, $v0                   # Sauvegarder le labyrinthe dans $s2

  # 2. Créer une pile de cellules ;
  move $a0, $s1                   # Préparer $a0 avec N*N
  jal st_creer                    # Créer de la pile de taille N*N
  move $s3, $v0                   # Sauvegarder la pile en $s2

  # 3. Soit C0 la cellule en haut à gauche du labyrinthe ;
  # 4. Faire de C0 la cellule courante ;
  li $s4, 0                       # Initialiser $s4 avec 0 qui est l'indice de la première celulle

  # 5. Marquer C0 comme visitée ;
  move $a0, $s2                   # Préparer $a0 avec l'adresse du labyrinthe
  move $a1, $s4                   # Préparer $a1 avec l'indice de la celulle courante
  jal definir_cellule_visitee     # Définir la celulle comme visitée

  # 6. Mettre C0 dans la pile ;
  move $a0, $s3                   # Préparer $a0 avec l'adresse de la pile
  move $a1, $s4                   # Préparer $a1 avec l'indice de la celulle courante
  jal st_empiler                  # Empiler l'indice de la celulle courante

  # 7. Tant que la pile n’est pas vide, répéter :
lg_loop:
  move $a0, $s3                   # Préparer $a0 avec l'adresse de la pile
  jal st_est_vide                 # Est-ce que la pile est vide ?
  beq $v0, 1, lg_end              # Si oui, finir

  #   • Rechercher les cellules voisines de la cellule courante C qui n’aient pas encore été visitées ;

  move $a0, $s2                   # Préparer $a0 avec l'adresse du labyrinthe
  move $a1, $s4                   # Préparer $a1 avec l'indice de la cellule courante
  jal lister_valides_directions   # Lister les directions valide de la cellue courante

  move $a0, $v0                   # Préparer $a0 avec les directions valides
  jal voisin_aleatoire            # Séléctionner une au hasard
  beq $v0, 0, lg_next             # Si il n'y a pas de voisins valides, passer à la suite

  #   • S’il y en a au moins une :
  #     — choisir une de ces cellules au hasard → C' ;
  #     — Casser le mur entre C et C' ;

  move $a0, $s2                   # Préparer $a0 avec l'adresse du labyrinthe
  move $a1, $s4                   # Préparer $a1 avec l'indice de la cellule courante
  move $a2, $v0                   # Prépare $a2 avec la direction séléctionnée
  jal casser_mur                  # Casser le mur

  #     — Faire de C' la cellule courante ;
  move $s4, $v0                   # La cellule voisine devient la cellule courante

  #     — Marquer C' comme visitée ;
  move $a0, $s2                   # Préparer $a0 avec l'adresse du labyrinthe
  move $a1, $s4                   # Préparer $a1 avec l'indice de la celulle courante
  jal definir_cellule_visitee     # Définir la celulle comme visitée

  #     — Mettre C' au sommet de la pile
  move $a0, $s3                   # Préparer $a0 avec l'adresse de la pile
  move $a1, $s4                   # Préparer $a1 avec l'indice de la celulle courante
  jal st_empiler                  # Empiler l'indice de la celulle courante
  j lg_loop                       # Reboucler

lg_next:
  #   • S’il n’y en a aucune :
  #     — Dépiler
  move $a0, $s3                   # Préparer $a0 avec l'adresse de la pile
  jal st_depiler                  # Dépiler

  #     — Soit C' la cellule au sommet de la pile
  #     — En faire la cellule courante
  move $a0, $s3                   # Préparer $a0 avec l'adresse de la pile
  jal st_sommet                   # Récupérer le sommet de pile
  move $s4, $v0                   # Pour le mettre dans la cellule courante
  j lg_loop                       # Reboucler

lg_end:                           # Fin de la boucle

  # 8. Faire de C0 la cellule d’entrée et de la cellule CN , en bas à droite, la cellule de sortie.
                                  # Définir la cellule de départ
  move $a0, $s2                   #   Mettre l'adresse du labyrinthe dans $a0
  li $a1, 0                       #   Mettre l'indice 0 dans $a1
  jal definir_cellule_depart      #   Définir la cellule de départ

                                  # Définir la cellule de fin
  move $a0, $s2                   #   Mettre l'adresse du labyrinthe dans a0
  move $a1, $s1                   #   Mettre N*N dans a1
  subi $a1, $a1, 1                #   Décrémenter a1 pour avoir N*N-1
  jal definir_cellule_fin         #   Définir la cellule de fin

  move $a0, $s2                   # Mettre l'adresse du labyrinthe dans a0
  jal labyrinthe_nettoyer         # Nettoyer le labyrinthe

  move $v0, $s2                   # Mettre l'adresse du labyrinthe dans v0
  pop_saved($s4)                  # Restaurer $s4
  pop_saved($s3)                  # Restaurer $s3
  pop_saved($s2)                  # Restaurer $s2
  pop_saved($s1)                  # Restaurer $s1
  pop_saved($s0)                  # Restaurer $s0
  pop_ra_and_return               # Restaurer $ra et retourner


# ============================================================================
# Test les fonction de generation du labyrinthe

# ----------------------------------------------------------------------------
test_definir_cellule_depart:
  push_ra
  push_saved($s0)

  li $a0, 2
  jal labyrinthe_creer
  move $s0, $v0

  move $a0, $s0
  li $a1, 0
  jal definir_cellule_depart

  move $a0, $s0
  li $a1, 0
  jal labyrinthe_lire_cellule

  move $a0, $s0
  check_eq ($v0, 31, "la cellule aurait du valoir 31")

  pop_saved($s0)
  pop_ra_and_return

# ----------------------------------------------------------------------------
test_definir_cellule_fin:
  push_ra
  push_saved($s0)

  li $a0, 2
  jal labyrinthe_creer
  move $s0, $v0

  move $a0, $s0
  li $a1, 0
  jal definir_cellule_fin

  move $a0, $s0
  li $a1, 0
  jal labyrinthe_lire_cellule

  move $a0, $s0
  check_eq ($v0, 47, "la cellule aurait du valoir 47")

  pop_saved($s0)
  pop_ra_and_return

# ----------------------------------------------------------------------------
test_definir_cellule_visitee:
  push_ra
  push_saved($s0)

  li $a0, 2
  jal labyrinthe_creer
  move $s0, $v0

  move $a0, $s0
  li $a1, 0
  jal definir_cellule_visitee

  move $a0, $s0
  li $a1, 0
  jal labyrinthe_lire_cellule

  move $a0, $s0
  check_eq ($v0, 79, "la cellule aurait du valoir 79")

  pop_saved($s0)
  pop_ra_and_return

# ----------------------------------------------------------------------------
test_inverser_direction:
  push_ra

  li $a0, Haut
  inverser_direction($a0)
  check_eq ($a0, Bas, "la direction aurait du valoir Bas")

  li $a0, Droite
  inverser_direction($a0)
  check_eq ($a0, Gauche, "la direction aurait du valoir Gauche")

  li $a0, Bas
  inverser_direction($a0)
  check_eq ($a0, Haut, "la direction aurait du valoir Haut")

  li $a0, Gauche
  inverser_direction($a0)
  check_eq ($a0, Droite, "la direction aurait du valoir Droite")

  pop_ra_and_return

# ----------------------------------------------------------------------------
test_casser_mur_droite:
  push_ra
  push_saved($s0)

  li $a0, 2
  jal labyrinthe_creer
  move $s0, $v0

  move $a0, $s0
  li $a1, 0
  li $a2, Droite
  jal casser_mur

  move $a0, $s0
  li $a1, 0
  jal labyrinthe_lire_cellule

  move $a0, $s0
  check_eq ($v0, 13, "la cellule aurait du valoir 13")

  move $a0, $s0
  li $a1, 1
  jal labyrinthe_lire_cellule

  move $a0, $s0
  check_eq ($v0, 7, "la cellule aurait du valoir 7")

  pop_saved($s0)
  pop_ra_and_return

# ----------------------------------------------------------------------------
test_casser_mur_gauche:
  push_ra
  push_saved($s0)

  li $a0, 2
  jal labyrinthe_creer
  move $s0, $v0

  move $a0, $s0
  li $a1, 1
  li $a2, Gauche
  jal casser_mur

  move $a0, $s0
  li $a1, 0
  jal labyrinthe_lire_cellule

  move $a0, $s0
  check_eq ($v0, 13, "la cellule aurait du valoir 13")

  move $a0, $s0
  li $a1, 1
  jal labyrinthe_lire_cellule

  move $a0, $s0
  check_eq ($v0, 7, "la cellule aurait du valoir 7")

  pop_saved($s0)
  pop_ra_and_return

# ----------------------------------------------------------------------------
test_casser_mur_bas:
  push_ra
  push_saved($s0)

  li $a0, 2
  jal labyrinthe_creer
  move $s0, $v0

  move $a0, $s0
  li $a1, 0
  li $a2, Bas
  jal casser_mur

  move $a0, $s0
  li $a1, 0
  jal labyrinthe_lire_cellule

  move $a0, $s0
  check_eq ($v0, 11, "la cellule aurait du valoir 11")

  move $a0, $s0
  li $a1, 2
  jal labyrinthe_lire_cellule

  move $a0, $s0
  check_eq ($v0, 14, "la cellule aurait du valoir 14")

  pop_saved($s0)
  pop_ra_and_return

# ----------------------------------------------------------------------------
test_casser_mur_haut:
  push_ra
  push_saved($s0)

  li $a0, 2
  jal labyrinthe_creer
  move $s0, $v0

  move $a0, $s0
  li $a1, 2
  li $a2, Haut
  jal casser_mur

  move $a0, $s0
  li $a1, 0
  jal labyrinthe_lire_cellule

  move $a0, $s0
  check_eq ($v0, 11, "la cellule aurait du valoir 11")

  move $a0, $s0
  li $a1, 2
  jal labyrinthe_lire_cellule

  move $a0, $s0
  check_eq ($v0, 14, "la cellule aurait du valoir 14")

  pop_saved($s0)
  pop_ra_and_return

# ----------------------------------------------------------------------------
suite_generation_labyrinthe:
  push_ra
  jal test_definir_cellule_depart
  jal test_definir_cellule_fin
  jal test_definir_cellule_visitee
  jal test_inverser_direction
  jal test_casser_mur_droite
  jal test_casser_mur_gauche
  jal test_casser_mur_bas
  jal test_casser_mur_haut
  pop_ra_and_return
