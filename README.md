# Projet archi labyrinthe

Pour generer un labyrinthe, utiliser le script generate

    ./generate 5

Pour lancer les tests et generer un labyrinthe de test :

    make run

Pour lancer une session de test en continue :

    ./watch

Cela lance une observation des sources et un lancement de make run quand un fichier change. Make run joue deux sons différents en cas de succès ou d'echecs des tests.
